export const CheckIcon = require('./icons/check.png');
export const FeedTabIconOff = require('./tabIcons/icFeedOff.png');
export const FeedTabIconOn = require('./tabIcons/movieFrames1492.png');
export const LocationsTabIconOff = require('./tabIcons/pinSharpCircle624.png');
export const LocationsTabIconOnrequire = require('./tabIcons/icPinSharpCircleOn.png');
export const MessagesTabIconOff = require('./tabIcons/icMessageOff.png');
export const MessagesTabIconOn = require('./tabIcons/icMessageOn.png');
export const PrivateIcon = require('./icons/private.png');

export const InstagramIcon = require('./icons/instagram.png');
