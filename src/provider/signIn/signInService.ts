import SignInConfig from './signInConfig';

export default class SignInService {
  static signIn(token: string) {
    return new Promise(async resolve => {
      console.log("I'm on services");

      const endpoint = SignInConfig.endpoint;
      const response = await fetch(`${endpoint}${token}`);
      const data = await response.json();

      resolve(data);
    });
  }
  static logInBackend(id: string) {
    return new Promise(async resolve => {
      try {
        console.log("I'm on services login");
        console.log(id);

        const body = {instagram_id: id};
        console.log(body);
        const endpoint = SignInConfig.logInBackend;
        console.log({endpoint, body});
        const response = await fetch(endpoint, {
          method: 'POST',
          body: JSON.stringify(body),
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const data = await response.json();
        console.log('aca la data del login backend', data);
        resolve(data);
      } catch (error) {
        //
      }
    });
  }
}
