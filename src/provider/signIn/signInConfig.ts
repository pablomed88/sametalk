import {APIConnector} from '..';
import config from '../../config';

const apiConnector = new APIConnector({timeout: 50000});

export default class SignInConfig {
  static get endpoint(): string {
    return `${config.API_URL_INSTAGRAM}`;
  }

  static get endpointBackend(): string {
    return `${config.API_URL}`;
  }

  static get endpointInstagram(): string {
    return `${SignInConfig.endpoint}/`;
  }

  static get logInBackend(): string {
    return `${SignInConfig.endpointBackend}/api/auth/login`;
  }

  static get APIConnector(): APIConnector {
    return apiConnector;
  }
}
