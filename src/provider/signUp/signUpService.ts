import SignUpConfig from './signUpConfig';
import AsyncStorage from '@react-native-community/async-storage';
import config from '../../config';

export default class SignUpService {
  static signUp({
    instagram_id,
    username,
    profile_picture,
    full_name,
    bio,
    is_bussines,
    follows,
    followed_by,
    birthday,
    gender,
    country_id,
  }: {
    instagram_id?: string;
    username?: string;
    profile_picture?: string;
    full_name?: string;
    bio?: string;
    is_bussines?: boolean;
    follows?: number;
    followed_by?: number;
    birthday?: string;
    gender?: string;
    country_id?: string;
  }): Promise<Object | string> {
    return new Promise(async resolve => {
      try {
        console.log("I'm on services sign up");
        console.log('que onda');

        const body = {
          instagram_id,
          username,
          profile_picture,
          full_name,
          bio,
          is_bussines,
          follows,
          followed_by,
          birthday,
          gender,
          country_id,
          coin: '0',
        };

        const endpoint = SignUpConfig.signUp;
        console.log(body);
        console.log(endpoint);
        const response = await fetch(endpoint, {
          method: 'POST',
          body: JSON.stringify(body),
          headers: {
            'Content-Type': 'application/json',
          },
        });
        const data = await response.json();
        console.log('dame la data de mi registro');
        console.log(data);
        resolve(data);
      } catch (error) {
        //
      }
    });
  }
}
