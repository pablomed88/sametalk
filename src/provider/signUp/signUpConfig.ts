import {APIConnector} from '..';
import config from '../../config';

const apiConnector = new APIConnector({timeout: 50000});

export default class SignUpConfig {
  static get endpoint(): string {
    return `${config.API_URL}`;
  }

  static get signUp(): string {
    return `${config.API_URL}/api/auth/register`;
  }

  static get endpointInstagram(): string {
    return `${SignUpConfig.endpoint}/`;
  }

  static get APIConnector(): APIConnector {
    return apiConnector;
  }
}
