import React from 'react';
import { TextStyle } from 'react-native';
import { StyledText } from './styles';
import { theme } from '../../styled';

export const TypographyVariant = {
  bold: 'nunito-bold',
  extraBold: 'nunito-extraBold',
  light: 'nunito-light',
  regular: 'nunito-regular',
};

export interface TypographyProps {
  color: keyof typeof theme.text;
  children: React.ReactNode;
  fontWeight: any;
  size: number;
  style: TextStyle;
  textAlign: 'center' | 'justify' | 'left' | 'right';
  variant: keyof typeof TypographyVariant;
}

const Typography = ({ children, ...props }: TypographyProps) =>
  !children ? null : <StyledText {...props}>{children}</StyledText>;

Typography.defaultProps = {
  color: 'primary',
  children: null,
  fontWeight: 'normal',
  size: 12,
  style: {},
  textAlign: 'left',
  variant: 'regular',
};

export default Typography;
