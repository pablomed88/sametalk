import { Text, TextProperties } from 'react-native';
import styled from 'styled-components/native';
import { TypographyVariant } from './Typography';
import { nlz, theme } from '../../styled';

type StyledTextProps = {
  color: keyof typeof theme.text;
  fontWeight: any;
  size: number;
  textAlign: 'center' | 'justify' | 'left' | 'right';
  variant: keyof typeof TypographyVariant;
} & TextProperties;

export const StyledText = styled(Text)((p: StyledTextProps) => ({
  color: theme.text[p.color],
  fontSize: nlz(p.size),
  fontWeight: p.fontWeight,
  textAlign: p.textAlign,
}));
