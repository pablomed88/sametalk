import React from 'react';

import {ButtonTouchable, Title} from './styles';
import LinearGradient from 'react-native-linear-gradient';

export type Type =
  | 'primary'
  | 'secondary'
  | 'secondaryWhite'
  | 'secondaryPink'
  | 'black';

export type Size = 'small' | 'medium' | 'big' | 'auto';

interface ButtonProps {
  disabled: boolean;
  onPress: () => void;
  text: string;
  type: Type;
  size: Size;
  textSize: number;
  bold: boolean;
}

const Button = ({
  bold,
  disabled,
  onPress,
  size,
  text,
  textSize,
  type,
}: ButtonProps) => (
  <ButtonTouchable
    disabled={disabled}
    onPress={onPress}
    size={size}
    type={type}>
    <Title bold={bold} size={textSize} type={type}>
      {text}
    </Title>
  </ButtonTouchable>
);

Button.defaultProps = {
  degraded: false,
  type: 'primary',
  disabled: false,
  onPress: () => {},
  size: 'medium',
  textSize: 16,
  bold: false,
};

export default Button;
