import { TextProps, TouchableOpacityProps } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled, { css } from 'styled-components/native';
import { Type, Size } from './Button';
import { rs } from '../../styled';

type TitleProps = {
  bold: boolean;
  size: number;
  type: Type;
} & TextProps;

type ButtonTouchableProps = {
  disabled: boolean;
  size: Size;
  type: Type;
} & TouchableOpacityProps;

const getTypeStyle = (type: Type, disabled: boolean) =>
  ({
    primary: css`
      background-color: ${p => (disabled ? p.theme.color.grayInactive : p.theme.color.pinkPrimary)};
    `,
    secondaryWhite: css`
      background-color: transparent;
      border-color: ${p => p.theme.color.white};
      border-width: ${rs(1)}px;
    `,
    secondaryPink: css`
      background-color: transparent;
      border-color: ${p => p.theme.color.pinkPrimary};
      border-width: ${rs(1)}px;
    `,
    black: css`
      background-color: ${p => (disabled ? p.theme.color.blackInactive : p.theme.color.black)};
    `,
  }[type]);

const getSizeStyle = (size: Size) =>
  ({
    small: css`
      min-width: 35%;
    `,
    medium: css`
      min-width: 60%;
    `,
    big: css`
      min-width: 85%;
    `,
    auto: css`
      /* fits to text width */
    `,
  }[size]);

function getTextStyle(type: Type) {
  return {
    primary: css`
      color: ${p => p.theme.color.white};
    `,
    secondaryWhite: css`
      color: ${p => p.theme.color.white};
    `,
    secondaryPink: css`
      color: ${p => p.theme.color.pinkPrimary};
    `,
    black: css`
      color: ${p => p.theme.color.white};
    `,
  }[type];
}

export const ButtonTouchable = styled.TouchableOpacity<ButtonTouchableProps>`
  border-radius: ${rs(27)}px;
  height: ${rs(55)}px;
  justify-content: center;
  padding: ${rs(5)}px;

  ${p => getTypeStyle(p.type, p.disabled)};
  ${p => getSizeStyle(p.size)};
`;

export const Title = styled.Text<TitleProps>`
  font-size: ${p => rs(p.size)}px;
  font-weight: ${p => (p.bold ? 'bold' : 'normal')};

  text-align: center;

  ${p => getTextStyle(p.type)}
`;
