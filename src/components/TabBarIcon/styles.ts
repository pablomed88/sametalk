import styled from 'styled-components';
import {Image} from 'react-native';
import {rs} from '../../styled';

const ICON_SIZE = rs(20);

export const IconImage = styled(Image)({
  height: ICON_SIZE,
  margin: rs(10),
  width: ICON_SIZE,
});
