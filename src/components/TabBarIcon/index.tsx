import React, {Component} from 'react';

import * as Routes from '../../navigation/routes';
import {
  FeedTabIconOff,
  FeedTabIconOn,
  LocationsTabIconOff,
  LocationsTabIconOn,
  MessagesTabIconOff,
  MessagesTabIconOn,
} from '../../assets/images';
import {IconImage} from './styles';

interface Props {
  focused: boolean;
  name: string;
}

class TabBarIcon extends Component<Props> {
  getImage = () => {
    const {focused, name} = this.props;
    switch (name) {
      case Routes.Home:
        return focused ? MessagesTabIconOn : MessagesTabIconOff;
      case Routes.Profile:
        return focused ? FeedTabIconOn : FeedTabIconOff;
      default:
        return null;
    }
  };

  render() {
    // @ts-ignore
    return <IconImage resizeMode="contain" source={this.getImage()} />;
  }
}

export default TabBarIcon;
