import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import {theme} from '../../styled';
import {
  View,
  Platform,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Typography} from '../../components';
import {rs} from '../../styled';
import {responsiveSize} from '../../utils/dimensions';

export const ButtonContainer = styled(View)<any>(() => ({
  flexDirection: 'row',
  borderColor: theme.color.pinkPrimary,
  borderRadius: rs(100),
  borderWidth: rs(1),
  background: theme.color.white,
  alignItems: 'center',
  justifyContent: 'center',
  top: rs(140),
  left: rs(45),
  width: rs(200),
}));

export const Container = styled(LinearGradient)({
  flex: 1,
  justifyContent: 'flex-end',
  alignItems: 'center',
  paddingBottom: responsiveSize(200),
});

export const BodyContainer = styled(View)({});
export const InterestContainer = styled(TouchableOpacity)({
  justifyContent: 'center',
  paddingLeft: rs(20),
  height: rs(70),
  width: rs(300),
  backgroundColor: 'yellow',
  marginTop: rs(5),
  marginBottom: rs(5),
});
export const Line = styled(View)<any>(() => ({
  backgroundColor: theme.color.grayishWhite,
  width: 1,
}));

export const Title = styled(Typography)<any>(() => ({
  fontWeight: 'bold',
  margin: rs(20),
  textAlign: 'center',
}));

export const WelcomeBackground = styled(ImageBackground)<any>(() => ({
  alignItems: 'center',
  flex: 1,
  justifyContent: 'center',
  width: '100%',
}));

export const WelcomeImage = styled(Image)<any>(() => ({
  bottom: Platform.OS === 'ios' ? rs(210) : rs(190),
  height: rs(60),
  width: rs(275),
}));
