import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Alert, FlatList, View, TouchableOpacity} from 'react-native';
import {ActivityIndicator, Modal} from 'react-native';
import {theme, rs} from '../../styled';
import {goToPage} from '../../navigation';
import {Button, Spacing, Typography} from '../../components';
import {signIn} from '../../actions/signIn';
import {
  BodyContainer,
  ButtonContainer,
  Container,
  InterestContainer,
} from './styles';
import {responsiveSize} from '../../utils/dimensions';
interface Props {
  logInConnected?: Function;
  loginData?: any;
  loginError?: any;
  loginLoading?: boolean;
  navigation?: any;
}
class SelectInterests extends React.Component<Props> {
  [x: string]: any;
  state = {token: '', data: [], isReg: []};

  async onRegister(token: string) {
    const {logInConnected} = this.props;
    await logInConnected(token);
    const {loginData, loginError} = this.props;
    if (loginError) {
      Alert.alert(
        'Error!',
        `${loginError.error_type}: ${loginError.error_message}`,
      );
    } else {
      const {loginLoading} = this.props;
      goToPage('Welcome');
    }
  }
  render() {
    console.log('aca en select');
    console.log(this.props);
    const {
      loginLoading,
      navigation: {
        state: {
          params: {origin},
        },
      },
    } = this.props;
    return (
      <Container
        colors={['white', 'white']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}>
        {loginLoading ? (
          <Modal
            animationType="slide"
            transparent={true}
            visible={loginLoading}>
            <ActivityIndicator size="large" color="#000" />
          </Modal>
        ) : (
          <BodyContainer>
            <FlatList
              style={{backgroundColor: 'red', top: origin ? rs(80) : rs(120)}}
              data={[
                {
                  id: 1,
                  name: 'Deportes',
                  children: [
                    {
                      id: 2,
                      name: 'Futbol',
                      children: [
                        {
                          id: 4,
                          name: 'River Plate',
                          children: [],
                        },
                        {
                          id: 5,
                          name: 'Boca',
                          children: [],
                        },
                      ],
                    },
                    {
                      id: 3,
                      name: 'Tenis',
                      children: [
                        {
                          id: 6,
                          name: 'JM Del Potro',
                          children: [],
                        },
                      ],
                    },
                  ],
                },
                {
                  id: 1,
                  name: 'Deportes',
                  children: [
                    {
                      id: 2,
                      name: 'Futbol',
                      children: [
                        {
                          id: 4,
                          name: 'River Plate',
                          children: [],
                        },
                        {
                          id: 5,
                          name: 'Boca',
                          children: [],
                        },
                      ],
                    },
                    {
                      id: 3,
                      name: 'Tenis',
                      children: [
                        {
                          id: 6,
                          name: 'JM Del Potro',
                          children: [],
                        },
                      ],
                    },
                  ],
                },
                {
                  id: 1,
                  name: 'Deportes',
                  children: [
                    {
                      id: 2,
                      name: 'Futbol',
                      children: [
                        {
                          id: 4,
                          name: 'River Plate',
                          children: [],
                        },
                        {
                          id: 5,
                          name: 'Boca',
                          children: [],
                        },
                      ],
                    },
                    {
                      id: 3,
                      name: 'Tenis',
                      children: [
                        {
                          id: 6,
                          name: 'JM Del Potro',
                          children: [],
                        },
                      ],
                    },
                  ],
                },
              ]}
              renderItem={({item}) => (
                <InterestContainer>
                  <Typography size={20}>{item.name}</Typography>
                </InterestContainer>
              )}
              keyExtractor={item => item.id}
            />
            {origin ? (
              <ButtonContainer>
                <Button
                  bold
                  onPress={() => goToPage('MainTabs')}
                  size="small"
                  text="Continue"
                  textSize={15}
                  type="secondary"
                />
              </ButtonContainer>
            ) : null}
          </BodyContainer>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  loginData: state.signIn.signInData,
  loginError: state.signIn.signInError,
  loginLoading: state.signIn.signInLoading,
});

const mapDispatchToProps = (dispatch: any): any =>
  bindActionCreators(
    {
      logInConnected: signIn,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectInterests);
