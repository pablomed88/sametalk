import styled from 'styled-components/native';

import {theme} from '../../styled';
import {View, Platform, Image, ImageBackground} from 'react-native';
import {Typography} from '../../components';
import {rs} from '../../styled';

export const ButtonContainer = styled(View)<any>(() => ({
  borderColor: theme.color.black,
  borderRadius: rs(100),
  borderWidth: rs(1),
  marginTop: rs(5),
  background: theme.color.white,
  alignItems: 'center',
  justifyContent: 'center',
  width: rs(200),
}));

export const Container = styled(View)(() => ({
  flex: 1,
  alignItems: 'center',
  backgroundColor: theme.color.white,
  justifyContent: 'center',
}));

export const Line = styled(View)<any>(() => ({
  backgroundColor: theme.color.grayishWhite,
  width: 1,
}));

export const Title = styled(Typography)<any>(() => ({
  fontWeight: 'bold',
  margin: rs(20),
  textAlign: 'center',
}));

export const WelcomeBackground = styled(ImageBackground)<any>(() => ({
  alignItems: 'center',
  flex: 1,
  justifyContent: 'center',
  width: '100%',
}));

export const WelcomeImage = styled(Image)<any>(() => ({
  bottom: Platform.OS === 'ios' ? rs(210) : rs(190),
  height: rs(60),
  width: rs(275),
}));
