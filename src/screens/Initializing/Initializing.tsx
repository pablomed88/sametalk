/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {ButtonContainer} from './styles';
import {goToPage} from '../../navigation';
import config from '../../config';
import AsyncStorage from '@react-native-community/async-storage';
import Swiper from 'react-native-swiper';
import {Typography, Button} from '../../components';
import {responsiveSize} from '../../utils/dimensions';

class Initializing extends React.Component {
  state = {};

  async componentDidMount() {
    const token = await AsyncStorage.getItem(config.USER_TOKEN);
    console.log(token);
    if (token) {
      goToPage('MainTabs');
    }
  }

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Swiper showsButtons={false} loop={false}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                width: '80%',
                height: '60%',
                marginTop: 70,
                marginBottom: 20,
                borderColor: 'black',
                borderWidth: 1,
              }}></View>
            <View
              style={{
                width: '80%',
              }}>
              <Typography textAlign="justify" size={16}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry’s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Typography>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                width: '80%',
                height: '60%',
                marginTop: responsiveSize(70),
                marginBottom: responsiveSize(20),
                borderColor: 'black',
                borderWidth: responsiveSize(1),
              }}></View>
            <View
              style={{
                width: '80%',
              }}>
              <Typography textAlign="justify" size={16}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry’s standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.
              </Typography>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <View
              style={{
                width: '80%',
                height: '60%',
                marginTop: responsiveSize(70),
                marginBottom: responsiveSize(20),
                borderColor: 'black',
                borderWidth: 1,
              }}></View>
            <View
              style={{
                width: '80%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Typography textAlign="justify" size={16}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry’s standard dummy
                text ever since the 1500s.
              </Typography>
              <ButtonContainer>
                <Button
                  bold
                  onPress={() => goToPage('SignIn')}
                  size="small"
                  text="Let's Go!"
                  textSize={24}
                  type="secondary"
                />
              </ButtonContainer>
            </View>
          </View>
        </Swiper>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Initializing;
