import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Alert, Image} from 'react-native';
import {ActivityIndicator, Modal} from 'react-native';
import {theme, rs} from '../../styled';
import InstagramLogin from 'react-native-instagram-login';
import {goToPage} from '../../navigation';
import {Button, Typography} from '../../components';
import {signIn} from '../../actions/signIn';
import {BodyContainer, ButtonContainer, Container} from './styles';
import {responsiveSize} from '../../utils/dimensions';
interface Props {
  logInConnected?: Function;
  loginData?: any;
  loginError?: any;
  loginLoading?: boolean;
}
class Notification extends React.Component<Props> {
  [x: string]: any;
  state = {token: '', data: [], isReg: []};

  async onRegister(token: string) {
    const {logInConnected} = this.props;
    await logInConnected(token);
    const {loginData, loginError} = this.props;
    if (loginError) {
      Alert.alert(
        'Error!',
        `${loginError.error_type}: ${loginError.error_message}`,
      );
    } else {
      goToPage('MainTabs');
    }
  }
  render() {
    const {loginLoading} = this.props;
    return (
      <Container
        colors={['white', 'white']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}>
        {loginLoading ? (
          <Modal
            animationType="slide"
            transparent={true}
            visible={loginLoading}>
            <ActivityIndicator size="large" color="#000" />
          </Modal>
        ) : (
          <BodyContainer>
            <Typography
              size={36}
              style={{bottom: responsiveSize(300)}}
              textAlign="center">
              Notification Screen
            </Typography>
            <ButtonContainer>
              <Button
                bold
                onPress={() => {
                  Alert.alert('You are in Notification screen');
                }}
                size="small"
                text="Press me"
                textSize={15}
                type="secondary"
              />
            </ButtonContainer>
          </BodyContainer>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  loginData: state.signIn.signInData,
  loginError: state.signIn.signInError,
  loginLoading: state.signIn.signInLoading,
});

const mapDispatchToProps = (dispatch: any): any =>
  bindActionCreators(
    {
      logInConnected: signIn,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);
