import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Alert, Image, View, TouchableOpacity} from 'react-native';
import {ActivityIndicator, Modal} from 'react-native';
import {theme, rs} from '../../styled';
import InstagramLogin from 'react-native-instagram-login';
import {goToPage} from '../../navigation';
import {Button, Spacing, Typography} from '../../components';
import {signIn} from '../../actions/signIn';
import {BodyContainer, ButtonContainer, Container} from './styles';
import {responsiveSize} from '../../utils/dimensions';
interface Props {
  logInConnected?: Function;
  loginData?: any;
  loginError?: any;
  loginLoading?: boolean;
}
class SignIn extends React.Component<Props> {
  [x: string]: any;
  state = {token: '', data: [], isReg: []};

  async onRegister(token: string) {
    const {logInConnected} = this.props;
    await logInConnected(token);
    const {loginData, loginError} = this.props;
    if (loginError) {
      Alert.alert(
        'Error!',
        `${loginError.error_type}: ${loginError.error_message}`,
      );
    } else {
      const {loginLoading} = this.props;
      goToPage('Welcome');
    }
  }
  render() {
    const {loginLoading} = this.props;
    return (
      <Container
        colors={['white', 'white']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}>
        <View
          style={{
            width: rs(200),
            height: rs(200),
            borderRadius: rs(200 / 3),
            borderColor: 'black',
            borderWidth: rs(1),
            bottom: rs(160),
          }}></View>
        {loginLoading ? (
          <Modal
            animationType="slide"
            transparent={true}
            visible={loginLoading}>
            <ActivityIndicator size="large" color="#000" />
          </Modal>
        ) : (
          <BodyContainer>
            <Typography
              size={36}
              style={{bottom: responsiveSize(160)}}
              textAlign="center">
              SameTalk
            </Typography>
            <ButtonContainer>
              <Image
                style={{width: rs(30), height: rs(30), right: rs(10)}}
                source={require('../../assets/images/icons/instagram2.png')}
              />
              <Button
                bold
                onPress={() => {
                  this.instagramLogin.show();
                }}
                size="small"
                text="SIGN IN WITH INSTAGRAM"
                textSize={15}
                type="secondary"
              />
            </ButtonContainer>
            <Spacing />

            <Typography size={15} textAlign="center">
              Don't have an account?
            </Typography>
            <TouchableOpacity
              onPress={() => {
                Alert.alert('Sign Up');
              }}>
              <Typography size={15} style={{color: 'blue'}} textAlign="center">
                Sign Up
              </Typography>
            </TouchableOpacity>

            <Spacing />

            <InstagramLogin
              ref={(ref: any) => (this.instagramLogin = ref)}
              clientId="c222a1cb5aa94671adc8c085a2d1aaf4"
              redirectUrl="https://google.com"
              scopes={['basic']}
              onLoginSuccess={(token: any) => this.onRegister(token)}
              cacheEnabled={false}
              incognito={true}
              thirdPartyCookiesEnabled={false}
              sharedCookiesEnabled={false}
              domStorageEnabled={false}
              wrapperStyle={{borderWidth: 0}}
              containerStyle={{borderRadius: 100 / 4}}
              closeStyle={{
                backgroundColor: 'black',
                borderWidth: 0,
                borderColor: 'blue',
              }}
            />
          </BodyContainer>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  loginData: state.signIn.signInData,
  loginError: state.signIn.signInError,
  loginLoading: state.signIn.signInLoading,
});

const mapDispatchToProps = (dispatch: any): any =>
  bindActionCreators(
    {
      logInConnected: signIn,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignIn);
