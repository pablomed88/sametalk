import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import {theme} from '../../styled';
import {View, Platform, Image, ImageBackground} from 'react-native';
import {Typography} from '../../components';
import {rs} from '../../styled';
import {responsiveSize} from '../../utils/dimensions';

export const ButtonContainer = styled(View)<any>(() => ({
  borderColor: theme.color.black,
  borderRadius: rs(100),
  borderWidth: rs(1),
  background: theme.color.white,
  alignItems: 'center',
  justifyContent: 'center',
  width: rs(350),
}));

export const Container = styled(LinearGradient)({
  flex: 1,
  alignItems: 'center',
  paddingBottom: responsiveSize(200),
});

export const BodyContainer = styled(View)({});

export const Line = styled(View)<any>(() => ({
  backgroundColor: theme.color.grayishWhite,
  width: 1,
}));

export const NacionalityContainer = styled(View)({
  flexDirection: 'row',
  right: 20,
  width: '80%',
});

export const ProfileImageContainer = styled(View)<any>(() => ({
  borderColor: theme.color.black,
  borderRadius: rs(300 / 2),
  borderWidth: rs(1),
  background: theme.color.white,
  width: rs(300),
  height: rs(300),
  marginTop: rs(60),
  alignItems: 'center',
  justifyContent: 'center',
}));

export const Title = styled(Typography)<any>(() => ({
  fontWeight: 'bold',
  margin: rs(20),
  textAlign: 'center',
}));

export const WelcomeBackground = styled(ImageBackground)<any>(() => ({
  alignItems: 'center',
  flex: 1,
  justifyContent: 'center',
  width: '100%',
}));

export const WelcomeImage = styled(Image)<any>(() => ({
  bottom: Platform.OS === 'ios' ? rs(210) : rs(190),
  height: rs(60),
  width: rs(275),
}));
