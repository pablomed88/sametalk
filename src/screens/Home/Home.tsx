import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Image} from 'react-native';
import {ActivityIndicator, Modal} from 'react-native';
import {goToPage} from '../../navigation';
import {Spacing, Typography} from '../../components';
import {signIn} from '../../actions/signIn';
import {
  BodyContainer,
  ButtonContainer,
  Container,
  NacionalityContainer,
  ProfileImageContainer,
} from './styles';
import {responsiveSize} from '../../utils/dimensions';
import {rs} from '../../styled';
import {TouchableOpacity} from 'react-native-gesture-handler';

interface Props {
  logInConnected?: Function;
  loginData?: any;
  loginError?: any;
  loginLoading?: boolean;
}

class Home extends React.Component<Props> {
  [x: string]: any;
  state = {token: '', data: [], isReg: []};

  render() {
    const {loginLoading} = this.props;
    return (
      <Container
        colors={['white', 'white']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}>
        {loginLoading ? (
          <Modal
            animationType="slide"
            transparent={true}
            visible={loginLoading}>
            <ActivityIndicator size="large" color="#0000ff" />
          </Modal>
        ) : (
          <BodyContainer>
            <ProfileImageContainer>
              <Image
                style={{
                  borderRadius: rs(300 / 2),
                  borderWidth: rs(1),
                  width: rs(300),
                  height: rs(300),
                }}
                source={{
                  uri:
                    'https://scontent.cdninstagram.com/vp/f15699bb9b5fb69a35b72795a5454414/5E40C239/t51.2885-19/s150x150/36824380_2168749130056455_5990581446287294464_n.jpg?_nc_ht=scontent.cdninstagram.com',
                }}
              />
            </ProfileImageContainer>
            <Typography size={36} style={{}} textAlign="center">
              Pablo, Medina
            </Typography>
            <Spacing />
            <TouchableOpacity>
              <Typography size={24} style={{}} textAlign="left">
                Likes
              </Typography>
            </TouchableOpacity>
            <Spacing />

            <TouchableOpacity>
              <Typography size={24} style={{}} textAlign="left">
                Languages
              </Typography>
            </TouchableOpacity>
          </BodyContainer>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  loginData: state.signIn.signInData,
  loginError: state.signIn.signInError,
  loginLoading: state.signIn.signInLoading,
});

const mapDispatchToProps = (dispatch: any): any =>
  bindActionCreators(
    {
      logInConnected: signIn,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
