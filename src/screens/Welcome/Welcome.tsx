import React from 'react';
import {Text, TouchableOpacity, View, Image, Alert} from 'react-native';
import CheckBox from 'react-native-check-box';
import CountryPicker from 'react-native-country-picker-modal';

import {reduxForm, InjectedFormProps} from 'redux-form';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Container, Line, FieldContainer, LogoContainer} from './styles';
import {goToPage} from '../../navigation';
import * as Form from '../../modules/form';

import {Typography, Button, TextInput, Spacing} from '../../components';
import {signUp} from '../../actions/signUp';

import * as yup from 'yup';
import {rs} from '../../styled';
import {ButtonContainer} from '../Initializing/styles';

interface Values {
  signUp: {
    age: string;
  };
}

type StoreProps = ReturnType<typeof mapStateToProps>;

type ConnectProps = StoreProps & {
  logIn: Function;
};

type FormProps = InjectedFormProps<Values, ConnectProps>;

type Props = ConnectProps &
  FormProps & {logOutConnected?: Function; signUpConnected?: Function};

interface State {}

const VALID_AGE = '';

class Welcome extends React.Component<Props> {
  state = {
    isCheckedM: false,
    isCheckedF: false,
    visibleCountryPicker: false,
    country: null,
  };
  componentDidMount() {
    console.log('aca en welcome');
    console.log(this.props);
    const {profileData} = this.props;

    if (profileData) {
      if (profileData.responseToken.status !== 'invalid_credentials') {
        Alert.alert('The user is registered');
        goToPage('MainTabs');
      } else {
        Alert.alert('The user is not registered');
      }
    }
  }

  sendData = async () => {
    console.log('send data');
    console.log(this.state);
    console.log(this.props);
    const {profileData, signUpForm} = this.props;

    const {isCheckedF, country} = this.state;
    let gender;
    if (isCheckedF) gender = 'F';
    else gender = 'M';

    const body = {
      instagram_id: profileData.response.data.id,
      username: profileData.response.data.username,
      profile_picture: profileData.response.data.profile_picture,
      full_name: profileData.response.data.full_name,
      bio: profileData.response.data.bio,
      is_bussines: profileData.response.data.is_business,
      follows: profileData.response.data.counts.follows,
      followed_by: profileData.response.data.counts.followed_by,
      birthday: signUpForm.values.age,
      gender,
      country_id: country.cca2,
    };
    console.log(body);
    const {signUpConnected} = this.props;
    await signUpConnected(body);
    Alert.alert('User register was successfully');

    goToPage('SelectInterests', {origin: 'SignUp'});
  };

  render() {
    console.log('aca en signin');
    console.log(this.state);
    console.log(this.props);
    const {profileData} = this.props;
    const {visibleCountryPicker, country} = this.state;
    return (
      <Container>
        <LogoContainer></LogoContainer>
        <Typography style={{top: rs(75)}} size={26}>
          SameTalk
        </Typography>
        <Typography style={{top: rs(75)}} size={18}>
          Join to the community
        </Typography>
        <FieldContainer>
          <Typography fontWeight="bold" size={14}>
            Born date
          </Typography>
          <Line />
          <View style={{width: '40%', bottom: rs(10)}}>
            <Form.TextField
              baseColor="rgb(17, 17, 17)"
              keyboardType="numeric"
              placeholder="Born date"
              name="age"
            />
          </View>
        </FieldContainer>
        <Spacing />
        <FieldContainer>
          <Typography
            fontWeight="bold"
            size={14}
            style={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              paddingRight: rs(10),
            }}>
            Country
          </Typography>
          <Line />

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'flex-start',
              paddingLeft: rs(10),
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({visibleCountryPicker: true});
              }}>
              <Text>
                {!country ? `Select your country` : `${country.name}`}
              </Text>
            </TouchableOpacity>
            {visibleCountryPicker ? (
              <CountryPicker
                visible
                onSelect={country => {
                  this.setState({country});
                }}
                onClose={() => {
                  this.setState({visibleCountryPicker: false});
                }}
              />
            ) : null}
          </View>
        </FieldContainer>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            top: rs(130),
            right: rs(90),
          }}>
          <Typography textAlign="left" size={20}>
            Gender
          </Typography>
          <View style={{flexDirection: 'row', left: rs(70)}}>
            <Text>Masculino</Text>
            <CheckBox
              onClick={() => {
                this.setState({
                  isCheckedM: !this.state.isCheckedM,
                });
              }}
              isChecked={this.state.isCheckedM}
              leftText="Masculino"
            />
          </View>
          <View style={{flexDirection: 'row', left: rs(70)}}>
            <Text>Femenino</Text>
            <CheckBox
              onClick={() => {
                this.setState({
                  isCheckedF: !this.state.isCheckedF,
                });
              }}
              isChecked={this.state.isCheckedF}
              leftText="Femenino"
            />
          </View>
        </View>
        <ButtonContainer style={{bottom: rs(100)}}>
          <Button
            bold
            onPress={() => {
              this.sendData();
            }}
            size="small"
            text="Next"
            textSize={15}
            type="secondary"
          />
        </ButtonContainer>
      </Container>
    );
  }
}

const VALIDATION_SCHEMA = yup.object().shape({
  age: yup.string().required('Your age is required'),
});

function mapStateToProps(store) {
  return {
    logOutLoading: store.logOut.logOutLoading,
    profileData: store.signIn.signInData,
    initialValues: {age: VALID_AGE},

    signUpForm: Form.mapFormToProps<Values>(store.form.signUp),
  };
}

const mapDispatchToProps = (dispatch: any): any =>
  bindActionCreators(
    {
      signUpConnected: signUp,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  reduxForm<Values, ConnectProps>({
    form: 'signUp',
    destroyOnUnmount: true,
    asyncValidate: Form.validator(VALIDATION_SCHEMA),
  })(Welcome),
);
