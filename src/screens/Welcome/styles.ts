import styled from 'styled-components/native';

import {theme} from '../../styled';
import {View, Platform, Image, ImageBackground} from 'react-native';
import {Typography} from '../../components';
import {rs} from '../../styled';
import {height} from 'window-size';

export const ButtonContainer = styled(View)<any>(() => ({
  borderColor: theme.color.grayishWhite,
  borderRadius: rs(100),
  borderWidth: rs(1),
  background: theme.color.white,
  alignItems: 'center',
  justifyContent: 'center',
  width: rs(350),
}));

export const Container = styled(View)(() => ({
  flex: 1,
  alignItems: 'center',
}));

export const FieldContainer = styled(View)(() => ({
  width: '70%',
  height: rs(60),
  flexDirection: 'row',
  borderWidth: 1,
  borderColor: theme.color.grayInactive,
  borderRadius: rs(40 / 3),
  alignContent: 'center',
  paddingRight: rs(30),
  paddingLeft: rs(30),
  alignItems: 'center',
  justifyContent: 'space-between',
  top: 100,
}));

export const Line = styled(View)<any>(() => ({
  borderColor: theme.color.grayInactive,
  borderWidth: 1,
  width: 1,
  height: rs(60),
}));

export const LogoContainer = styled(View)<any>(() => ({
  borderColor: theme.color.grayInactive,
  borderWidth: 1,
  width: rs(170),
  height: rs(170),
  borderRadius: rs(170 / 2),
  top: rs(70),
}));

export const Title = styled(Typography)<any>(() => ({
  fontWeight: 'bold',
  margin: rs(20),
  textAlign: 'center',
}));

export const WelcomeBackground = styled(ImageBackground)<any>(() => ({
  alignItems: 'center',
  flex: 1,
  justifyContent: 'center',
  width: '100%',
}));

export const WelcomeImage = styled(Image)<any>(() => ({
  bottom: Platform.OS === 'ios' ? rs(210) : rs(190),
  height: rs(60),
  width: rs(275),
}));
