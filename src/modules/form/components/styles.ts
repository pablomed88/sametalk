import styled from 'styled-components';
import { Image } from 'react-native';

import { rs } from '../../../styled';

const ACCESSORY_SIZE = rs(20);

export const Accessory = styled(Image)({
  height: ACCESSORY_SIZE,
  width: ACCESSORY_SIZE,
  alignSelf: 'center',
});
