import React from 'react';
import { TextInputProps } from 'react-native';
import { TextFieldProps } from 'react-native-material-textfield';
import { WrappedFieldProps, Field, BaseFieldProps } from 'redux-form';
import { Accessory } from './styles';
import { TextInput } from '../../../components';
import { CheckIcon } from '../../../assets/images/';
interface TextFieldCustomProps {
  autoCapitalize?: boolean;
  autoFocus?: boolean;
  baseColor?: string;
  keyboardType?: string;
  label?: string;
  onBlur?: () => void;
  onFocus?: () => void;
  placeholder?: string;
  row: boolean;
  textColor?: string;
  tintColor?: string;
}

export type TextInputFieldProps = WrappedFieldProps &
  TextInputProps &
  TextFieldProps &
  TextFieldCustomProps;

class TextInputField extends React.Component<TextInputFieldProps> {
  renderAccesory = () => {
    const { input, meta } = this.props;
    return (
      <Accessory resizeMode="contain" source={input.value && meta.valid ? CheckIcon : undefined} />
    );
  };

  render() {
    const {
      input,
      meta,
      autoCapitalize,
      autoFocus,
      baseColor,
      keyboardType,
      label,
      onBlur,
      onFocus,
      placeholder,
      row,
      textColor,
      tintColor,
      ...textInputProps
    } = this.props;
    return (
      <TextInput
        {...textInputProps}
        autoCapitalize={autoCapitalize}
        autoFocus={autoFocus}
        baseColor={baseColor}
        error={meta.error && meta.touched ? meta.error : ''}
        keyboardType={keyboardType}
        label={label}
        onBlur={input.onBlur}
        onChangeText={input.onChange}
        onFocus={(e: any) => input.onFocus(e)} // workaround
        placeholder={placeholder}
        renderAccessory={this.renderAccesory}
        row={row}
        textColor={textColor}
        tintColor={tintColor}
        value={input.value}
      />
    );
  }
}

type ReduxFormPropsToPick =
  | 'format'
  | 'forwardRef'
  | 'immutableProps'
  | 'name'
  | 'normalize'
  | 'parse'
  | 'validate'
  | 'warn';

type PickedReduxFormProps = Pick<BaseFieldProps, ReduxFormPropsToPick>;

// NOTE: add more react-native TextInput props if needed
type ReactNativeTextInputPropsToPick =
  | 'autoCapitalize'
  | 'keyboardType'
  | 'maxLength'
  | 'secureTextEntry';

type PickedReactNativeTextInputProps = Pick<TextInputProps, ReactNativeTextInputPropsToPick>;

type MaterialTextInputPropsToPick = 'renderAccessory';

type PickedMaterialTextInputProps = Pick<TextFieldProps, MaterialTextInputPropsToPick>;

interface Props {
  autoCapitalize?: boolean;
  autoFocus?: boolean;
  baseColor?: string;
  keyboardType?: string;
  label?: string;
  onBlur?: () => void;
  onFocus?: () => void;
  placeholder?: string;
  row?: boolean;
  textColor?: string;
  tintColor?: string;
}

export const TextField: React.FC<Props> = props => <Field component={TextInputField} {...props} />;
