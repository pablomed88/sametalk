import {createAction} from 'redux-actions';
import {SIGNUP_LOADING, SIGNUP_ERROR, SIGNUP_SUCCESS} from './types';

export const singInLoading = createAction(SIGNUP_LOADING, () => true);
export const singInSuccess = createAction(SIGNUP_SUCCESS, data => data);
export const singInError = createAction(SIGNUP_ERROR, error => error);

import SignUpService from '../provider/signUp/signUpService';

export function signUp({
  instagram_id,
  username,
  profile_picture,
  full_name,
  bio,
  is_bussines,
  follows,
  followed_by,
  birthday,
  gender,
  country_id,
}: {
  instagram_id?: string;
  username?: string;
  profile_picture?: string;
  full_name?: string;
  bio?: string;
  is_bussines?: boolean;
  follows?: number;
  followed_by?: number;
  birthday?: string;
  gender?: string;
  country_id?: string;
}) {
  return async (dispatch: Function, getState: Function) => {
    dispatch({
      type: SIGNUP_LOADING,
      status: true,
    });
    console.log('estando en el action');
    const body = {
      instagram_id,
      username,
      profile_picture,
      full_name,
      bio,
      is_bussines,
      follows,
      followed_by,
      birthday,
      gender,
      country_id,
    };
    console.log(body);

    const response: any = await SignUpService.signUp(body);
    console.log('saliendo sign up');
    console.log(response);
    dispatch({
      type: SIGNUP_LOADING,
      status: false,
    });

    dispatch({
      type: SIGNUP_SUCCESS,
      data: response,
    });
  };
}
