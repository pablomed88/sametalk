import {createAction} from 'redux-actions';
import config from '../config';
import {SIGNIN_LOADING, SIGNIN_ERROR, SIGNIN_SUCCESS} from './types';

export const singInLoading = createAction(SIGNIN_LOADING, loading => true);
export const singInSuccess = createAction(SIGNIN_SUCCESS, data => data);
export const singInError = createAction(SIGNIN_ERROR, error => error);

import SignInService from '../provider/signIn/signInService';
import AsyncStorage from '@react-native-community/async-storage';

export function signIn(token: string) {
  return async (dispatch: Function, getState: Function) => {
    dispatch({
      type: SIGNIN_LOADING,
      status: true,
    });

    const response: any = await SignInService.signIn(token);
    console.log('dame mi data');
    console.log(response);
    const responseToken: any = await SignInService.logInBackend(
      response.data.id,
    );
    console.log('dame mi token');
    console.log(responseToken);
    dispatch({
      type: SIGNIN_LOADING,
      status: false,
    });

    if (response.meta.code !== 200) {
      dispatch({type: SIGNIN_ERROR, error: response.meta});
    } else {
      await AsyncStorage.setItem(
        config.USER_TOKEN,
        responseToken.status === 'invalid_credentials'
          ? token
          : responseToken.token,
      );
      dispatch({
        type: SIGNIN_SUCCESS,
        data: {response, responseToken},
      });
    }
  };
}
