// EXAMPLE ────────────────────────────────────────────────────────────────────────────────
export const FETCH_EXAMPLE_START = 'FETCH_EXAMPLE_START';
export const FETCH_EXAMPLE_SUCCESS = 'FETCH_EXAMPLE_SUCCESS';
export const FETCH_EXAMPLE_ERROR = 'FETCH_EXAMPLE_ERROR';

// LOGIN ────────────────────────────────────────────────────────────────────────────────
export const SIGNIN_LOADING = 'SIGNIN_LOADING';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const SIGNIN_ERROR = 'SIGNIN_ERROR';

// SIGN UP ────────────────────────────────────────────────────────────────────────────────
export const SIGNUP_LOADING = 'SIGNUP_LOADING';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';

// LOGIN OUT ────────────────────────────────────────────────────────────────────────────────
export const LOGOUT_LOADING = 'LOGOUT_LOADING';

// GENERAL ────────────────────────────────────────────────────────────────────────────────
export const ERASE_ALL_DATA = 'ERASE_ALL_DATA';
