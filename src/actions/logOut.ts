import {createAction} from 'redux-actions';
import config from '../config';
import {LOGOUT_LOADING, ERASE_ALL_DATA} from './types';

export const logOutLoading = createAction(LOGOUT_LOADING, loading => true);

import AsyncStorage from '@react-native-community/async-storage';

export function logOut() {
  return async (dispatch: Function, getState: Function) => {
    console.log('actions');
    dispatch({
      type: LOGOUT_LOADING,
      status: true,
    });
    await AsyncStorage.removeItem(config.USER_TOKEN);
    dispatch({
      type: ERASE_ALL_DATA,
    });

    dispatch({
      type: LOGOUT_LOADING,
      status: false,
    });
  };
}
