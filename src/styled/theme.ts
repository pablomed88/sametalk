const pinkPrimary = '#ec6f68';

const color = {
  imperial: '#633669',
  oldBurgundy: '#3F303E',
  black: 'black',
  blackInactive: 'rgba(0,0,0,0.05)',
  blue: '#0076FF',
  grayInactive: '#b2b2b2',
  pinkTabButtons: '#f18c85',
  pinkPrimary,
  cream: '#feddaa',
  softPink: '#e96566',
  white: 'white',
  whiteInactive: 'rgba(255,255,255,0.5)',
  grayishWhite: '#EAEAEA',
};

export const theme = {
  color,
  text: {
    black: 'black',
    darkGray: '#4a4a4a',
    lightGray: '#a4a4a4',
    pink: pinkPrimary,
    primary: 'black',
    red: '#EB6664',
    white: 'white',
    whiteInactive: 'rgba(255,255,255,0.5)',
  },
  ui: {
    error: 'red',
    primary: pinkPrimary,
  },
};

export type Theme = typeof theme;
