import {createAppContainer} from 'react-navigation';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';

import * as Routes from './routes';
import {AuthStack} from './AuthNavigation';
import {MainStack} from './MainNavigation';

const AppRoutes = {
  [Routes.AuthStack]: AuthStack,
  [Routes.MainStack]: MainStack,
};

const AppStack = createAnimatedSwitchNavigator(AppRoutes, {
  initialRouteName: Routes.AuthStack,
});

export default createAppContainer(AppStack);

export type Routes = keyof typeof Routes;
