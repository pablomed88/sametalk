import {createStackNavigator} from 'react-navigation-stack';

import MainTabNavigator from './MainTabNavigator';
import * as Routes from './routes';
import {SelectInterests} from '../screens';

const noHeader = {header: null};

const stack = {
  [Routes.MainTabs]: MainTabNavigator,
  [Routes.SelectInterests]: {
    screen: SelectInterests,
    navigationOptions: {title: 'Likes'},
  },
};

export const MainStack = createStackNavigator(stack);
