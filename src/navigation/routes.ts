// -- SWITCH ROUTES (Stacks) --

export const AuthStack = 'AuthStack';
export const MainStack = 'MainStack';

// -- SCREEN ROUTES --

// Auth Stack Routes
export const Initializing = 'Initializing';
export const SignIn = 'SignIn';
export const Welcome = 'Welcome';

export const Onboarding = 'Onboarding';

// Main Stack Routes
export const FetchExample = 'FetchExample';
export const MainTabs = 'MainTabs';

// MainTabs Routes
export const Profile = 'Profile';
export const Home = 'Home';
export const SelectInterests = 'SelectInterests';
export const Notification = 'Notification';
export const Messages = 'Messages';
export const Feed = 'Feed';
export const Locations = 'Locations';
