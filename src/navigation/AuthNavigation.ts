import {createStackNavigator} from 'react-navigation-stack';

import * as Routes from './routes';

import {
  Home,
  Initializing,
  Profile,
  SelectInterests,
  SignIn,
  Welcome,
} from '../screens';

const noHeader = {header: null};

const stack = {
  [Routes.Initializing]: {
    screen: Initializing,
    navigationOptions: noHeader,
  },
  [Routes.SignIn]: {
    screen: SignIn,
    navigationOptions: noHeader,
  },
  [Routes.Welcome]: {
    screen: Welcome,
    navigationOptions: noHeader,
  },
  [Routes.Profile]: {
    screen: Profile,
    navigationOptions: noHeader,
  },
  [Routes.Home]: {
    screen: Home,
    navigationOptions: noHeader,
  },
  [Routes.SelectInterests]: {
    screen: SelectInterests,
    navigationOptions: noHeader,
  },
};

export const AuthStack = createStackNavigator(stack);
