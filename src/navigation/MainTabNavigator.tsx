import React from 'react';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import * as Routes from './routes';
import {
  Home,
  Messages,
  Notification,
  Profile,
  SelectInterests,
} from '../screens';
import {TabBarIcon} from '../components';
import {theme, rs} from '../styled';

const createTabBarIcon = (name: any) => (iconProps: any) => (
  <TabBarIcon name={name} {...iconProps} />
);
const noHeader = {header: null};

const MainTabNavigator = createBottomTabNavigator(
  {
    [Routes.Home]: {
      screen: Home,
      navigationOptions: {
        //tabBarIcon: createTabBarIcon(Routes.Home),
        tabBarLabel: 'H',
      },
    },
    [Routes.Notification]: {
      screen: Notification,
      navigationOptions: {
        //tabBarIcon: createTabBarIcon(Routes.Profile),
        tabBarLabel: 'N',
      },
    },
    [Routes.Messages]: {
      screen: Messages,
      navigationOptions: {
        //tabBarIcon: createTabBarIcon(Routes.Profile),
        tabBarLabel: 'M',
      },
    },
    [Routes.Profile]: {
      screen: Profile,
      navigationOptions: {
        //tabBarIcon: createTabBarIcon(Routes.Profile),
        tabBarLabel: 'P',
      },
    },
  },

  {
    tabBarOptions: {
      activeTintColor: theme.color.pinkTabButtons,
      inactiveTintColor: theme.color.grayInactive,
      labelStyle: {
        fontSize: 28,
        margin: 0,
        padding: 0,
      },
    },
  },
);

export default MainTabNavigator;
