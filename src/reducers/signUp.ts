// @flow

import {
  SIGNUP_ERROR,
  SIGNUP_LOADING,
  SIGNUP_SUCCESS,
  ERASE_ALL_DATA,
} from '../actions/types';

const initialState = {
  signInData: null,
  signInError: null,
  signInLoading: false,
};

export default (state: Object = initialState, action: any): Object => {
  switch (action.type) {
    case SIGNUP_LOADING:
      return Object.assign({}, state, {signInLoading: action.status});
    case SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        signInData: action.data,
      });
    case SIGNUP_ERROR:
      return Object.assign({}, state, {
        signInError: action.error,
      });
    case ERASE_ALL_DATA:
      return initialState;
    default:
      return state;
  }
};
