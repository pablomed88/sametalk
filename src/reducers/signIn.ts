// @flow

import {
  SIGNIN_ERROR,
  SIGNIN_SUCCESS,
  SIGNIN_LOADING,
  ERASE_ALL_DATA,
} from '../actions/types';

const initialState = {
  signInData: null,
  signInError: null,
  signInLoading: false,
};

export default (state: Object = initialState, action: any): Object => {
  switch (action.type) {
    case SIGNIN_LOADING:
      return Object.assign({}, state, {signInLoading: action.status});
    case SIGNIN_SUCCESS:
      return Object.assign({}, state, {
        signInData: action.data,
      });
    case SIGNIN_ERROR:
      return Object.assign({}, state, {
        signInError: action.error,
      });
    case ERASE_ALL_DATA:
      return initialState;
    default:
      return state;
  }
};
