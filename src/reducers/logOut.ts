// @flow

import {LOGOUT_LOADING} from '../actions/types';

const initialState = {
  logOutLoading: false,
};

export default (state: Object = initialState, action: any): Object => {
  switch (action.type) {
    case LOGOUT_LOADING:
      return Object.assign({}, state, {signInLoading: action.status});

    default:
      return state;
  }
};
