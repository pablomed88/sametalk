import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import signIn from './signIn';
import logOut from './logOut';

const State = combineReducers({
  signIn,
  logOut,
  form: formReducer,
});

export default State;
